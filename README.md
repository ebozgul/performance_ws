# README #

### Running the nodes and plotting ###

* Run run.sh 
* The plots of the message rates will appear in Rplots.pdf

### Results ###

* The tests were conducted over 6 different loop rates (100, 1000, 10000, 100000, 1000000, 10000000)
* Each combination is represented with a different color
	* cpp2cpp - red
	* py2cpp - blue
	* cpp2py - green
	* py2py  - magenta
* Actual loop rate starts falling below the desired after 1000 hz
* Python publisher cannot exceed 2500 hz, which is significantly lower than CPP publisher