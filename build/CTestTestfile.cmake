# CMake generated Testfile for 
# Source directory: /home/etkin/performance_ws/src
# Build directory: /home/etkin/performance_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(performance_tests)
