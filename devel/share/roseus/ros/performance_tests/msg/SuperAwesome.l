;; Auto-generated. Do not edit!


(when (boundp 'performance_tests::SuperAwesome)
  (if (not (find-package "PERFORMANCE_TESTS"))
    (make-package "PERFORMANCE_TESTS"))
  (shadow 'SuperAwesome (find-package "PERFORMANCE_TESTS")))
(unless (find-package "PERFORMANCE_TESTS::SUPERAWESOME")
  (make-package "PERFORMANCE_TESTS::SUPERAWESOME"))

(in-package "ROS")
;;//! \htmlinclude SuperAwesome.msg.html


(defclass performance_tests::SuperAwesome
  :super ros::object
  :slots (_data ))

(defmethod performance_tests::SuperAwesome
  (:init
   (&key
    ((:data __data) "")
    )
   (send-super :init)
   (setq _data (string __data))
   self)
  (:data
   (&optional __data)
   (if __data (setq _data __data)) _data)
  (:serialization-length
   ()
   (+
    ;; string _data
    4 (length _data)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _data
       (write-long (length _data) s) (princ _data s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _data
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _data (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;;
   self)
  )

(setf (get performance_tests::SuperAwesome :md5sum-) "992ce8a1687cec8c8bd883ec73ca41d1")
(setf (get performance_tests::SuperAwesome :datatype-) "performance_tests/SuperAwesome")
(setf (get performance_tests::SuperAwesome :definition-)
      "string data 

")



(provide :performance_tests/SuperAwesome "992ce8a1687cec8c8bd883ec73ca41d1")


