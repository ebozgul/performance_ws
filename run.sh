#!/bin/bash

source devel/setup.bash

roscore &
rosrun performance_tests SuperAwesomeListener &

sleep 5


rates=(100 1000 10000 100000 1000000 10000000)
waits=(10 10 10 10 10 10)


echo "cpp publisher"

for i in {0..5}
do
    echo "rate ${rates[$i]}"
    rosrun performance_tests SuperAwesomePublisher _rate:=${rates[$i]} &
    sleep ${waits[$i]}
    rosnode kill /SuperAwesomePublisher
done


echo "py publisher"
for i in {0..5}
do
    echo "rate ${rates[$i]}"
    rosrun performance_tests SuperAwesomePublisher.py ${rates[$i]} &
    sleep ${waits[$i]}
    sleep 0.5
    rosnode list
    rosnode kill /SuperAwesomePublisher_py
done

sleep 1
rosnode kill /SuperAwesomeListener


killall -9 rosmaster

roscore &
sleep 2

echo "py listener"
rosrun performance_tests SuperAwesomeListener.py &

sleep 5

echo "cpp publisher"
for i in {0..5}
do
    echo "rate ${rates[$i]}"
    rosrun performance_tests SuperAwesomePublisher _rate:=${rates[$i]} &
    sleep ${waits[$i]}
    rosnode kill /SuperAwesomePublisher
done

sleep 1
echo "py publisher"

for i in {0..5}
do
    echo "rate ${rates[$i]}"

    rosrun performance_tests SuperAwesomePublisher.py ${rates[$i]} &

    sleep ${waits[$i]}
    sleep 0.5
    rosnode list
    rosnode kill /SuperAwesomePublisher_py
done



sleep 1
killall -9 rosmaster

Rscript plot_perf.R

