#!/usr/bin/env python
import rospy
from std_msgs.msg import String


pyout = open('pyout.csv','w')

def callback(data):
    now = rospy.get_rostime()
    pyout.write("%s,%s.%s\n" % ( data.data,now.secs,now.nsecs ))

def SuperAwesomeListener():
    pyout.write("pub,rate,id,pub_time,rec_time\n")
    rospy.init_node('SuperAwesomeListener_py', anonymous=True)
    rospy.Subscriber("SuperAwesomeTopic", String, callback)

    rospy.spin()
    pyout.close()

if __name__ == '__main__':
    SuperAwesomeListener()
