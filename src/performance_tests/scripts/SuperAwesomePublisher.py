#!/usr/bin/env python
import rospy
from std_msgs.msg import String
import sys

def SuperAwesomePublisher(rate):
    pub = rospy.Publisher('SuperAwesomeTopic', String, queue_size=100)
    rospy.init_node('SuperAwesomePublisher_py', anonymous=False)
    loopRate = rospy.Rate(rate)
    count = 0
    while not rospy.is_shutdown():
        now = rospy.get_rostime()
        msgStr = "py,%s,%s,%s.%s" % (rate,count,now.secs,now.nsecs)
        #rospy.loginfo(msgStr)
        count += 1
        pub.publish(msgStr)
        loopRate.sleep()

if __name__ == '__main__':

    if len(sys.argv) < 2:
        SuperAwesomePublisher(10)
    else:
        SuperAwesomePublisher(int(sys.argv[1]))


