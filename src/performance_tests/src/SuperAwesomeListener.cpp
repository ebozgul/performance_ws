#include "ros/ros.h"
#include "std_msgs/String.h"
#include <fstream>


std::ofstream out;
void callback(const std_msgs::String::ConstPtr &msg){
    out<<msg->data.c_str()<<","<<ros::Time::now()<<"\n";
}


int main(int argc, char **argv){

    ros::init(argc, argv, "SuperAwesomeListener");

    ros::NodeHandle n;

    out.open("cppout.csv");
    out<<"pub,rate,id,pub_time,rec_time\n";
    ros::Subscriber sub = n.subscribe("SuperAwesomeTopic", 1000, callback);
    
    ros::spin();
    out.close();
    return 0;
}

