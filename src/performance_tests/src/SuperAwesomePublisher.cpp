#include "ros/ros.h"
#include "std_msgs/String.h"
#include "performance_tests/SuperAwesome.h"
#include <sstream>



int main(int argc, char **argv){

	ros::init(argc,argv,"SuperAwesomePublisher");
	ros::NodeHandle n;
    ros::Publisher pub = n.advertise<performance_tests::SuperAwesome>("SuperAwesomeTopic",1000);
    
    int rate;
    ros::NodeHandle privateN("~");
    privateN.param("rate",rate, int(10));

    ros::Rate loopRate(rate);
    int count = 0;
    while(ros::ok()){
        performance_tests::SuperAwesome msg;
        std::stringstream ss;
        ss << "cpp,"<<rate<<","<< count<<","<<ros::Time::now();
        msg.data = ss.str();

        //ROS_INFO("%s", msg.data.c_str());
        
        pub.publish(msg);

        ros::spinOnce();

        loopRate.sleep();
        count++;
    }
    return 0;
}


